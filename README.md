This test automation project uses the next frameworks:

Allure - for report creation
TestNG - for tests execution, as well was used data provider for data-driven testing (FractionDataProvider)
Rest-Assured - rest client with easy syntax for testing
JSONAssert - library for json comparison expected vs actual

As a template for all test classes was written BaseRest class.
It has setUp method with initialization of rest client properties and wraps its methods,
such as get, post, delete. Plus it has additional helper methods.

As a place to store URI for requests was created ServicesURI.
For data driven testing was written TestDataReader which reads files and return
iterator for TestNG FractionDataProvider.
Then this data provider pass a set of different data combinations for tests in FractionsTest.

In FractionsTest was scripted tests for Get Fraction and Add Fraction functionality.
In Add Fraction functionality was found an issue when post request with existing profile data
may work as put and thus overwrite data, what would be not correct.

Test suite would be executed from command line with "mvn clean test" command,
and report generated with "mvn site"
Configuration for test suite was placed in testng.xml, which is mentioned in pom.xml in surefire plugin.
As well in pom.xml was placed system properties with rest server address and port,
which will be used by BaseRest after execution.
