package rest;

import org.testng.annotations.Test;
import rest.common.BaseRest;
import rest.common.ServicesURI;
import rest.testdata.FractionDataProvider;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

@Features("Fraction tests")
public class FractionsTest extends BaseRest {

    @Test(dataProvider = "dataForFractionRead", dataProviderClass = FractionDataProvider.class)
    @Title("Read fractions")
    @Description("Read of fraction profile")
    @Severity(SeverityLevel.CRITICAL)
    public void readFractions(@Parameter String description, @Parameter String profileName,
                              @Parameter String expectedJSONOfProfile, String expectedResponseCode) {
        get(ServicesURI.FRACTIONS + "/" + profileName)
                .then().assertThat().statusCode(Integer.parseInt(expectedResponseCode));

        JSONSoftAssert(expectedJSONOfProfile, responseWithoutEscapes());
    }

    @Test(dataProvider = "dataForFractionCreation", dataProviderClass = FractionDataProvider.class)
    @Title("Create fractions")
    @Description("Creation of fraction profile with different data combinations from JSON")
    @Severity(SeverityLevel.CRITICAL)
    public void addFractions(@Parameter String description, String profileName, @Parameter String JSON, String expectedResponseCode) {
        post(JSON, ServicesURI.FRACTIONS)
                .then().assertThat().statusCode(Integer.parseInt(expectedResponseCode));

        if (expectedResponseCode.equals("200")) {
            get(ServicesURI.FRACTIONS + "/" + profileName);
            JSONSoftAssert(JSON, responseWithoutEscapes());
        }
    }

}
