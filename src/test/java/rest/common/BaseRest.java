package rest.common;


import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.testng.annotations.BeforeClass;


public class BaseRest {

    private Response response;

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = System.getProperty("baseRestServerAddress");
        RestAssured.port = Integer.parseInt(System.getProperty("baseRestServerPort"));
        RestAssured.requestSpecification = new RequestSpecBuilder().addHeader("Content-Type", "application/json").build();

    }

    public Response post(String JSON, String URI) {
        response = RestAssured.given().body(JSON).when().post(URI);
        return response;
    }

    public Response get(String URI) {
        response = RestAssured.get(URI);
        return response;
    }

    public Response delete(String URI) {
        response = RestAssured.delete(URI);
        return response;
    }

    public String responseWithoutEscapes() {
        return response.asString().replaceAll("\\\\", "");
    }

    public void JSONSoftAssert(String expectedJSON, String actualJSON) {
        try {
            JSONAssert.assertEquals(expectedJSON, actualJSON, false);
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

}
