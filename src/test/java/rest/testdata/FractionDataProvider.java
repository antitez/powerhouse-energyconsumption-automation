package rest.testdata;

import org.testng.annotations.DataProvider;
import rest.common.BaseRest;
import rest.common.ServicesURI;
import rest.common.TestDataReader;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class FractionDataProvider {

    private final List<String> profilesToDeleteBeforeCreation = Arrays.asList("A", "B", "C", "D", "E", "F", "G");

    @DataProvider
    public Iterator<Object[]> dataForFractionCreation() {
        BaseRest rest = new BaseRest();
        //clean up all profiles before test
        for (String profile : profilesToDeleteBeforeCreation) {
            rest.delete(ServicesURI.FRACTIONS + "/" + profile);
        }

        //create profile which will be tried to update through create request, then it should return 400
        rest.post("[{\"fraction\":0.4,\"monthProfileKey\":{\"month\":\"JAN\",\"profile\":\"G\"}},{\"fraction\":0.3,\"monthProfileKey\":{\"month\":\"FEB\",\"profile\":\"G\"}},{\"fraction\":0.3,\"monthProfileKey\":{\"month\":\"MAR\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"APR\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"MAY\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"JUN\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"JUL\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"AUG\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"SEP\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"OCT\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"NOV\",\"profile\":\"G\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"DEC\",\"profile\":\"G\"}}]", ServicesURI.FRACTIONS);

        TestDataReader file = new TestDataReader("testdata/DataForFractionsProfileCreation.txt", "\\|");
        return file.iterator();
    }

    @DataProvider
    public Object[][] dataForFractionRead() {
        String XYZProfile = "[{\"fraction\":0.4,\"monthProfileKey\":{\"month\":\"JAN\",\"profile\":\"XYZ\"}},{\"fraction\":0.3,\"monthProfileKey\":{\"month\":\"FEB\",\"profile\":\"XYZ\"}},{\"fraction\":0.3,\"monthProfileKey\":{\"month\":\"MAR\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"APR\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"MAY\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"JUN\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"JUL\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"AUG\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"SEP\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"OCT\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"NOV\",\"profile\":\"XYZ\"}},{\"fraction\":0,\"monthProfileKey\":{\"month\":\"DEC\",\"profile\":\"XYZ\"}}]";

        new BaseRest().post(XYZProfile, ServicesURI.FRACTIONS);

        return new Object[][]{{"Existing profile", "XYZ", XYZProfile, "200"}, {"Not existing profile", "XYX", "[]", "200"}};
    }

}
